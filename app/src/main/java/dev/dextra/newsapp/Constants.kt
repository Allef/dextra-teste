package dev.dextra.newsapp

val API_KEY_HEADER_NAME = "X-Api-Key"
val API_KEY = "3ecc37a65de942e29d006b5cf6a09c52"
val BASE_URL = "https://newsapi.org"
val ICON_LOCATOR_URL = "https://icon-locator.herokuapp.com/icon?url=%s&size=30..60..150"
val IMAGE_LOCATOR_URL = "https://icon-locator.herokuapp.com/icon?url=%s&size=100..300..500"