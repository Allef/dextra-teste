package dev.dextra.newsapp.feature.news

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import dev.dextra.newsapp.R
import dev.dextra.newsapp.api.model.Article
import dev.dextra.newsapp.api.model.Source
import dev.dextra.newsapp.base.BaseListActivity
import dev.dextra.newsapp.feature.news.adapter.ArticleListAdapter
import kotlinx.android.synthetic.main.activity_news.*
import org.koin.android.ext.android.inject

class NewsActivity : BaseListActivity(), ArticleListAdapter.ArticlesAdapterItemListener {

    private val newsViewModel: NewsViewModel by inject()

    // TODO: Adjust specific texts
    override val emptyStateTitle: Int = R.string.empty_state_title_news
    override val emptyStateSubTitle: Int = R.string.empty_state_subtitle_news
    override val errorStateTitle: Int = R.string.error_state_title_news
    override val errorStateSubTitle: Int = R.string.error_state_subtitle_news
    override val mainList: View
        get() = news_list

    private var articleListAdapter: ArticleListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_news)
        super.onCreate(savedInstanceState)

        (intent?.extras?.getSerializable(NEWS_ACTIVITY_SOURCE) as Source).let { source ->
            title = source.name

            loadNews(source)
        }
    }

    private fun loadNews(source: Source) {
        newsViewModel.configureSource(source)

        articleListAdapter = ArticleListAdapter()
        articleListAdapter?.onArticlesAdapterItemListener = this
        news_list.layoutManager = LinearLayoutManager(this)
        news_list.adapter = articleListAdapter

        newsViewModel.articles?.observe(this, Observer { articles ->
            articleListAdapter?.submitList(articles)
        })

        newsViewModel.networkState?.observe(this, Observer { networkState ->
            articleListAdapter?.networkState = networkState
        })

        newsViewModel.initialLoading?.observe(this, networkStateObserver)
    }

    override fun onArticleClicked(article: Article) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(article.url)
        startActivity(intent)
    }

    override fun setupLandscape() = Unit
    override fun setupPortrait() = Unit

    override fun executeRetry() {
        newsViewModel.retry()
    }

    companion object {
        private const val NEWS_ACTIVITY_SOURCE = "NEWS_ACTIVITY_SOURCE"

        fun getIntent(context: Context, source: Source): Intent {
            return Intent(context, NewsActivity::class.java).apply {
                putExtra(NEWS_ACTIVITY_SOURCE, source)
            }
        }
    }
}
