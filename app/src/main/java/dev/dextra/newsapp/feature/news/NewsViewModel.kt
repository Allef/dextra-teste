package dev.dextra.newsapp.feature.news

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import dev.dextra.newsapp.api.model.Article
import dev.dextra.newsapp.api.model.Source
import dev.dextra.newsapp.api.repository.NewsRepository
import dev.dextra.newsapp.base.BaseViewModel
import dev.dextra.newsapp.base.NetworkState
import dev.dextra.newsapp.feature.news.paging.ArticlesDataFactory
import java.util.concurrent.Executors

class NewsViewModel(private val newsRepository: NewsRepository) : BaseViewModel() {

    private val executor = Executors.newFixedThreadPool(5)
    private var source: Source? = null
    private var articlesDataFactory: ArticlesDataFactory? = null

    var networkState: LiveData<NetworkState>? = null
    var initialLoading: LiveData<NetworkState>? = null
    var articles: LiveData<PagedList<Article>>? = null

    fun configureSource(source: Source) {
        this.source = source
        setupPaging(source)
    }

    private fun setupPaging(source: Source) {
        articlesDataFactory = ArticlesDataFactory(newsRepository, source)
        networkState = Transformations.switchMap(articlesDataFactory!!.dataSourceLiveData) {
            dataSource -> dataSource.networkState
        }

        initialLoading = Transformations.switchMap(articlesDataFactory!!.dataSourceLiveData) {
            dataSource -> dataSource.initialLoading
        }

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(10) // number of items to load initially.
            .setPageSize(NewsRepository.DEFAULT_PAGE_SIZE)
            .setPrefetchDistance(2) // number of preloads that occur
            .build()

        articles = LivePagedListBuilder(articlesDataFactory!!, pagedListConfig)
            .setInitialLoadKey(NewsRepository.FIRST_PAGE)
            .setFetchExecutor(executor)
            .build()
    }

    fun retry() {
        articlesDataFactory?.dataSourceLiveData?.value?.retryAllFailed()
    }
}
