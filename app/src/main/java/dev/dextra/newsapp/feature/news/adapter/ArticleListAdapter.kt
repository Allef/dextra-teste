package dev.dextra.newsapp.feature.news.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import dev.dextra.newsapp.R
import dev.dextra.newsapp.api.model.Article
import dev.dextra.newsapp.base.NetworkState
import kotlinx.android.synthetic.main.item_article.view.*
import kotlinx.android.synthetic.main.item_network_state.view.*
import java.lang.Exception
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ArticleListAdapter: PagedListAdapter<Article, RecyclerView.ViewHolder>(Article.DIFF_CALLBACK) {

    private val dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.SHORT)
    private val parseFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())

    var onArticlesAdapterItemListener: ArticlesAdapterItemListener? = null

    var networkState: NetworkState? = null
        set(value) {
            val previousState = field
            val previousExtraRow = hasExtraRow()
            field = value
            onNetworkStateChanged(previousState, previousExtraRow)
        }

    companion object {
        private const val TYPE_PROGRESS = 0
        private const val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return if (viewType == TYPE_PROGRESS) {
            val view =  layoutInflater.inflate(R.layout.item_network_state, parent, false)
            NetworkStateItemViewHolder(view)
        } else {
            val view =  layoutInflater.inflate(R.layout.item_article, parent, false)
            ArticleItemViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArticleItemViewHolder) {
            getItem(position)?.let { article ->
                holder.bindTo(article)
            }
        } else {
            (holder as NetworkStateItemViewHolder).bindView(networkState)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            TYPE_PROGRESS
        } else {
            TYPE_ITEM
        }
    }

    private fun hasExtraRow(): Boolean {
        return networkState != null && networkState !== NetworkState.SUCCESS
    }

    private fun onNetworkStateChanged(previousState: NetworkState?, previousExtraRow: Boolean) {
        val newExtraRow = hasExtraRow()
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(itemCount)
            } else {
                notifyItemInserted(itemCount)
            }
        } else if (newExtraRow && previousState !== networkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    interface ArticlesAdapterItemListener {
        fun onArticleClicked(article: Article)
    }

    inner class ArticleItemViewHolder(private val view: View):
        RecyclerView.ViewHolder(view) {

        fun bindTo(article: Article) {
            itemView.article_name.text = article.title
            itemView.article_description.text = article.description
            itemView.article_author.text = article.author
            itemView.article_date.text = dateFormat.format(parseFormat.parse(article.publishedAt))

            if(article.urlToImage?.isNotEmpty() == true) {
                Picasso.get().load(article.urlToImage)
                    .into(itemView.article_image, object : Callback {
                        override fun onSuccess() {
                            itemView.article_image.visibility = View.VISIBLE
                        }

                        override fun onError(e: Exception?) {
                            itemView.article_image.visibility = View.GONE
                        }
                    })
            } else {
                itemView.article_image.visibility = View.GONE
            }

            view.setOnClickListener{onArticlesAdapterItemListener?.onArticleClicked(article)}
        }
    }

    inner class NetworkStateItemViewHolder(view: View):
        RecyclerView.ViewHolder(view) {

        fun bindView(networkState: NetworkState?) {
            if (networkState != null && networkState === NetworkState.RUNNING) {
                itemView.progress_bar.visibility = View.VISIBLE
            } else {
                itemView.progress_bar.visibility = View.GONE
            }

            if (networkState != null && networkState === NetworkState.ERROR) {
                itemView.error_msg.visibility = View.VISIBLE
                itemView.error_msg.text = itemView.context.getString(R.string.error_load_articles)
            } else {
                itemView.error_msg.visibility = View.GONE
            }
        }
    }
}