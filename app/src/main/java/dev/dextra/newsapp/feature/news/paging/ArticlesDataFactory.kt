package dev.dextra.newsapp.feature.news.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import dev.dextra.newsapp.api.model.Article
import dev.dextra.newsapp.api.model.Source
import dev.dextra.newsapp.api.repository.NewsRepository
import java.util.concurrent.Executors

/**
 * @author michaels on 2019-05-18.
 */
class ArticlesDataFactory(
    private val newsRepository: NewsRepository,
    private val source: Source
): DataSource.Factory<Long, Article>() {

    private val retryExecutor = Executors.newFixedThreadPool(2)
    val dataSourceLiveData = MutableLiveData<ArticlesDataSource>()

    override fun create(): DataSource<Long, Article> {
        val feedDataSource = ArticlesDataSource(newsRepository, source, retryExecutor)
        dataSourceLiveData.postValue(feedDataSource)
        return feedDataSource
    }
}