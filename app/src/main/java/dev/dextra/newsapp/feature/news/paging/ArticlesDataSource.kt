package dev.dextra.newsapp.feature.news.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import dev.dextra.newsapp.api.model.Article
import dev.dextra.newsapp.api.model.Source
import dev.dextra.newsapp.api.repository.NewsRepository
import dev.dextra.newsapp.base.NetworkState
import io.reactivex.disposables.Disposable
import retrofit2.adapter.rxjava2.Result.response
import java.util.concurrent.Executor


/**
 * @author michaels on 2019-05-18.
 */
class ArticlesDataSource(private val newsRepository: NewsRepository,
                         private val source: Source,
                         private val retryExecutor: Executor
): PageKeyedDataSource<Long, Article>() {

    val networkState = MutableLiveData<NetworkState>()
    val initialLoading = MutableLiveData<NetworkState>()

    // keep a function reference for the retry event
    private var retry: (() -> Any)? = null

    var onFeedDataSourceListener: OnFeedDataSourceListener? = null

    interface OnFeedDataSourceListener {
        fun onLoadArticles(disposable: Disposable)
    }

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            retryExecutor.execute {
                it.invoke()
            }
        }
    }

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, Article>) {
        networkState.postValue(NetworkState.RUNNING)

        val disposable = newsRepository.getEverything(source.id)
            .subscribe({ response ->
                if (response.articles.isNotEmpty()) {
                    networkState.postValue(NetworkState.SUCCESS)
                    initialLoading.postValue(NetworkState.SUCCESS)

                    callback.onResult(response.articles,
                        null,
                        NewsRepository.FIRST_PAGE + 1)
                } else {
                    initialLoading.postValue(NetworkState.EMPTY)
                    networkState.postValue(NetworkState.EMPTY)
                }
                retry = null
            }, {
                networkState.postValue(NetworkState.ERROR)
                initialLoading.postValue(NetworkState.ERROR)

                retry = {
                    loadInitial(params, callback)
                }
            })

        onFeedDataSourceListener?.onLoadArticles(disposable)
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Article>) {
        networkState.postValue(NetworkState.RUNNING)

        val disposable = newsRepository.getEverything(source.id, params.key, params.requestedLoadSize)
            .subscribe({ response ->
                if (response.articles.isNotEmpty()) {
                    networkState.postValue(NetworkState.SUCCESS)

                    val nextKey = if(params.key == response.totalResults.toLong())
                        null
                    else
                        params.key + 1

                    callback.onResult(response.articles,
                        nextKey)
                } else {
                    networkState.postValue(NetworkState.EMPTY)
                }
                retry = null
            }, {
                networkState.postValue(NetworkState.ERROR)
                retry = {
                    loadAfter(params, callback)
                }
            })

        onFeedDataSourceListener?.onLoadArticles(disposable)
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Article>) = Unit
}