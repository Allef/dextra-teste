package dev.dextra.newsapp.feature.sources

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import dev.dextra.newsapp.TestConstants
import dev.dextra.newsapp.api.model.Article
import dev.dextra.newsapp.api.model.ArticlesResponse
import dev.dextra.newsapp.api.model.Source
import dev.dextra.newsapp.base.BaseTest
import dev.dextra.newsapp.base.TestSuite
import dev.dextra.newsapp.feature.news.NewsViewModel
import dev.dextra.newsapp.utils.JsonUtils
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.get

/**
 * @author michaels on 2019-05-19.
 */
class NewsViewModelTest : BaseTest() {

    @Suppress("unused")
    @get:Rule // used to make all live data calls sync
    val instantExecutor = InstantTaskExecutorRule()

    val emptyResponse = ArticlesResponse(ArrayList(), "ok", 0)
    val source = Source("general",
        "us",
        "Test description",
        "test-news",
        "en",
        "Test News",
        "http://test.com")

    lateinit var viewModel: NewsViewModel

    @Before
    fun setupTest() {
        viewModel = TestSuite.get()
    }

    @Test
    fun testEmptyNews() {
        TestSuite.mock(TestConstants.newsURL).body(JsonUtils.toJson(emptyResponse)).apply()

        viewModel.configureSource(source)
        assertNotNull(viewModel.articles)
        // TODO: Get paged list from datasource
    }
}